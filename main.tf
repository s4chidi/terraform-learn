provider "aws" {
    region = "us-east-1"
    access_key = "AKIAZWXFEEEZO6SEA2NN"
    secret_key = "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"
}

variable "subnet_cidr_block" {
    description= "subnet cidr block" 
}
  

resource "aws_vpc" "dev-vpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
        name: "dev"
        
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.dev-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = "us-east-1a"
    tags = {
        name: "subnet-1-dev"
    }
}

data "aws_vpc" "existing_vpc" {
    default = true 
}

resource "aws_subnet" "dev-subnet-2" {
    vpc_id = data.aws_vpc.existing_vpc.id 
    cidr_block = "172.31.96.0/20"
    availability_zone = "us-east-1a"
    tags = {
        name: "subnet-2-default"
    }
} 